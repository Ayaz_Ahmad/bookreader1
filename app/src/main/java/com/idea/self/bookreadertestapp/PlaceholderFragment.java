package com.idea.self.bookreadertestapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private static final String ARG_PAGE_NUMBER = "page_number";

    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setTextSize(20);
        int page_number_to_go = 1;
        page_number_to_go = getArguments().getInt(ARG_SECTION_NUMBER);
        goToPage(textView, page_number_to_go);
        return rootView;
    }

    public void goToPage(TextView textView, int page_number) {
        switch (page_number) {
            case 1:
                textView.setText(getString(R.string.page1));
                // toolbar.setTitle("صفحة رقم 1");
                break;
            case 2:
                textView.setText(getString(R.string.page2));
                // toolbar.setTitle("صفحة رقم 2");
                break;
            case 3:
                textView.setText(getString(R.string.page3));
                // toolbar.setTitle("صفحة رقم 3");
                break;
            case 4:
                textView.setText(getString(R.string.page4));
                break;
            case 5:
                textView.setText(getString(R.string.page5));
                break;
            case 6:
                textView.setText(getString(R.string.page6));
                break;
            case 7:
                textView.setText(getString(R.string.page7));
                break;
            case 8:
                textView.setText(getString(R.string.page8));
                break;
            case 9:
                textView.setText(getString(R.string.page9));
                break;
        }
    }
}
